# Malá

A brief description of what this project does and who it's for.

## Name

Your project name.

## Description

A longer description of your project and its purpose.

## Badges

![Build Status](https://img.shields.io/badge/build-passing-brightgreen)

## Visuals

Include a screenshot or a GIF of your project.

## Installation

```sh
# Instructions for setting up your project locally.
git clone https://github.com/yourusername/your-repo.git
cd your-repo
